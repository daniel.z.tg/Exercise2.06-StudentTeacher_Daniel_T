import org.junit.Test;

@SuppressWarnings("ALL")
public class School {

	//	public static void main(String[] args) {
	@Test
	public void test() {
		Student jamie = new Student("Jamie", "Larock", "Kanata", 76, "s123456789");
		Teacher james = new Teacher("James", "McGillvray", "Earl of March", "Phys Ed, Math, Science", "c12345");

		//print(jamie);
		//print(james);

		jamie.setStudentId("s87654321"); //Note: bad input, error checking should stop change
		james.setEmployeeId("C4321"); //Another bad output...

		print(jamie);
		print(james);

		jamie.setFirstName("Georgio");  //Note: this only exists in the superclass but we can use it!
		james.setFirstName("George");

		print(jamie);
		print(james);

		print("Jamie has been late " + jamie.getNumLates() + " times.");
		james.markStudentLate(jamie);
		print("Jamie has been late " + jamie.getNumLates() + " times.");

		/*
		 * Declare 3 new objects of type Person, but initialize them as one Person,
		 * one tudent and one Teacher.
		 */

		Person p1 = new Person("John", "Smith", "Hello Avenue");
		Person p2 = new Student("Molly", "Peters");
		Person p3 = new Teacher("Harold", "Wonkers");

		// REMEMBER: printing an object will call upon that object's
		// toString method automatically!

		print(p1);
		print(p2);
		print(p3);

		//Note: from this output you can already see that the constructor
		//initialization is what counts, i.e. even though Molly was
		//declared of type Person, she is now considered a student.

		// All three objects were declared using a superclass but two
		// of them were initialized using a subclass - when their methods
		// are called, the compiler will look for the method in the
		// subclass first, if it cannot find a method there then it will
		// look in the superclass

		// What if we try to instantiate the other way around

		// Student p4 = new Person("Molly","Peters");
		// Teacher p5 = new Person("Harold","Wonkers");
		//This doesn't work.  Remember the rectangle superclass and
		//the square subclass. Running the Person constructor
		//is not enough to initialize our Student and Teacher Objects here
		//and in fact this doesn't make sense.

		//Let's use some methods that are defined in person...

		print(p1.getLastName() + ", " + p1.getFirstName());
		print(p2.getLastName() + ", " + p2.getFirstName());
		print(p3.getLastName() + ", " + p3.getFirstName());

		//		print(p2.getStudentId());    //causes a syntax error.
		// Since p2 was declared as a Person object, p2 can only call upon
		// the methods it is guaranteed to have (the methods that exist
		// in the Person class)

		// But we know that p2 is really a Student object
		// by casting it to a Student object first, we will then be
		// able to call upon the methods that exist only in Student

		print(((Student) p2).getStudentId());

		// Any Person object could be cast to a Student or a Teacher
		// What happens if we cast p3 as a Student and call upon 
		// getStudentId method?

		// print(((Student)p3).getStudentId());
		// causes a ClassCastException so it might need to be inside of
		// a try and catch block

		// Eventually we might have an entire list of Person objects,
		// some of which are initialized as Teacher and some as Student
		// Can we use try and catch to find out what it is?

		try {
			print(((Student) p3).getStudentId()); // we know it is a Student
		} catch (ClassCastException e) {
			try {
				print(((Teacher) p3).getEmployeeId()); // we know it is a Teacher
			} catch (ClassCastException ex) {
				print(p3.getFirstName() + " is neither a student nor a teacher.");
			}
		}

		// an alternative approach to check for an object's type is to
		// use instanceof
		if (p3 instanceof Student) {
			print(p3.getFirstName() + " is a Student.");
			// it would be safe to cast t to a Student here and call
			// upon methods in the Student class
		} else {
			print(p3.getFirstName() + " is not a Student.");
		}
	}

	public static void print(Object o) { //Note: if we had String specified here
		System.out.println(o); //this would work for our string print statements
		System.out.println(); //but not for our objects
	}

}