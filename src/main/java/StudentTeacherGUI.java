import me.danielzgtg.compsci11_sem2_2017.common.appcontainer.impl.AttributeDBGraphicalAppContainer;
import me.danielzgtg.compsci11_sem2_2017.common.datastructure.CommonLambdas;
import me.danielzgtg.compsci11_sem2_2017.common.datastructure.MemoryDB;
import me.danielzgtg.compsci11_sem2_2017.common.platform.PlatformUtils;

public final class StudentTeacherGUI {

	@SuppressWarnings({"unchecked", "JavaDoc"})
	public static void main(final String[] ignore) {
		PlatformUtils.setHighPerformance(true);

		if (PlatformUtils.getHighPerformance()) {
			//SwingUtils.setupSystemLookAndFeel(); // TODO DEBUG
		}

		final MemoryDB<Person> db = new MemoryDB<>();

		db.addFirst(new Person("John", "Smith"));
		db.addLast(new Student());
		db.add(new Teacher());

		(new AttributeDBGraphicalAppContainer("School Database", CommonLambdas.NOP_CONSUMER,
				StudentTeacherCommonUI.ATTRIBUTES, db, "(C) 2017 Daniel Tang", Person.class)).launch();
	}

	@SuppressWarnings("JavaDoc")
	private StudentTeacherGUI() { throw new UnsupportedOperationException(); }
}
