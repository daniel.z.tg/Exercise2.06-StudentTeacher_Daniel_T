import me.danielzgtg.compsci11_sem2_2017.common.Validate;

/**
 * Describes one Teacher
 */
@SuppressWarnings({"WeakerAccess", "unused", "SameParameterValue"})
public class Teacher extends Person {
	private String teachables;
	private String employeeId;

	public Teacher() {
		super();

		this.setTeachables("");
		this.setEmployeeId("");
	}

	public Teacher(final String firstName, final String lastName) {
		super(firstName, lastName);

		this.setTeachables("");
		this.setEmployeeId("");
	}

	public Teacher(final String firstName, final String lastName,
			final String teachables, final String employeeId) {
		super(firstName, lastName);

		this.setTeachables(teachables);
		this.setEmployeeId(employeeId);
	}

	public Teacher(final String firstName, final String lastName,
			final String address, final String teachables, final String employeeId) {
		super(firstName, lastName, address);

		this.setTeachables(teachables);
		this.setEmployeeId(employeeId);
	}

	public Teacher(final String firstName, final String lastName,
			final String address, final Date birthday,
			final String teachables, final String employeeId) {
		super(firstName, lastName, address, birthday);

		this.setTeachables(teachables);
		this.setEmployeeId(employeeId);
	}

	public String getTeachables() {
		return this.teachables;
	}

	public String getEmployeeId() {
		return this.employeeId;
	}

	public void setTeachables(final String teachables) {
		Validate.notNull(teachables);

		this.teachables = teachables;
	}

	public void setEmployeeId(final String employeeId) {
		this.employeeId = isValidId(employeeId) ? employeeId : INVALID_ID_PLACEHOLDER;
	}

	private boolean isValidId(final String employeeId) {
		Validate.notNull(employeeId);

		return employeeId.length() == (1 + 5) && employeeId.startsWith("C");
	}

	public void markStudentLate(final Student s) {
		Validate.notNull(s);

		s.addLate();
	}

	@Override
	public String toString() {
		return "Teacher: firstName = " + this.getFirstName()
				+ ", lastName = " + this.getLastName()
				+ ", address = " + this.getAddress()
				+ ", birthday = " + this.getBirthday()
				+ ", age = " + this.getAge()
				+ ", teachables = " + this.teachables
				+ ", employeeId = " + this.employeeId;

	}

	@Override
	public boolean equals(final Object o){
		return o instanceof Teacher && this.equals((Teacher) o);
	}

	@Override
	public boolean equals(final Person p) {
		return p instanceof Teacher && this.equals((Teacher) p);
	}

	public boolean equals(final Teacher t) {
		return t != null && super.equals(t)
				&& this.teachables.equals(t.teachables)
				&& this.employeeId.equals(t.employeeId);
	}

	public int hashCode() {
		return super.hashCode() ^ this.teachables.hashCode() ^ this.employeeId.hashCode();
	}
}
