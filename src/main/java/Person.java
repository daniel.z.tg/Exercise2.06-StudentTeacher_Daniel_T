import java.time.LocalDate;

import me.danielzgtg.compsci11_sem2_2017.common.Validate;

/**
 * Describe one person. Each person will have a first name, a last name
 * and an address.
 *
 * @author Ms Cianci, Daniel Tang
 * @since Friday, October 30, 2009
 */
public class Person {
	private String firstName;
	private String lastName;
	private String address;
	private Date birthday;

	public static final String INVALID_ID_PLACEHOLDER = "invalid id";

	public Person() {
		this("", "", "");
	}

	public Person(final String first, final String last) {
		this(first, last, "");
	}

	public Person(final String first, final String last, final Date birthday) {
		this(first, last, "", birthday);
	}

	public Person(final String first, final String last, final String address) {
		this(first, last, address, Date.UNIX_EPOCH);
	}

	public Person(final String first, final String last, final String address, final Date birthday) {
		this.setFirstName(first);
		this.setLastName(last);
		this.setAddress(address);
		this.setBirthday(birthday);
	}

	/**
	 * Accessor method to get this person's first name.
	 *
	 * @return this person's first name
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Accessor method to get this person's last name.
	 *
	 * @return this person's last name
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Accessor method to get this person's address
	 *
	 * @return this person's address
	 */
	public String getAddress() {
		return address;
	}

	public Date getBirthday() {
		return this.birthday;
	}

	/**
	 * Mutator method to set this person's first name
	 *
	 * @param n string used to set the first name
	 */
	public void setFirstName(final String n) {
		Validate.notNull(n);

		firstName = n;
	}

	/**
	 * Mutator method to set this person's last name
	 *
	 * @param n string used to set the last name
	 */
	public void setLastName(final String n) {
		Validate.notNull(n);

		lastName = n;
	}

	/**
	 * Mutator method to set this person's address
	 *
	 * @param a string used to set the address
	 */
	public void setAddress(final String a) {
		Validate.notNull(a);

		address = a;
	}

	public void setBirthday(final Date birthday) {
		Validate.notNull(birthday);

		this.birthday = birthday;
	}

	public final int getAge() {
		return LocalUtil.getAge(this.getBirthday().asJavaDate(), LocalDate.now());
	}

	public final int getAge(final Date date) {
		return LocalUtil.getAge(this.getBirthday().asJavaDate(), date.asJavaDate());
	}

	/**
	 * Returns a string representation of this Person and its actual first
	 * name, last name and address.
	 *
	 * @return a string representation of this Person
	 */
	public String toString() {
		return "Person: firstName = " + this.firstName
				+ ", lastName = " + this.lastName
				+ ", address = " + this.address
				+ ", birthday = " + this.birthday
				+ ", age = " + this.getAge();
	}

	@Override
	public boolean equals(final Object o) {
		return o instanceof Person && this.equals((Person) o);
	}

	/**
	 * Compares this Person to the specified object. The result is true if
	 * the argument (p) has the same first name and last name as this object
	 *
	 * @param p the object to compare this Person to
	 * @return true if the Person objects are equal; false otherwise
	 */
	public boolean equals(final Person p) {
		return p != null && this.firstName.equalsIgnoreCase(p.firstName)
				&& this.lastName.equalsIgnoreCase(p.lastName)
				&& this.address.equalsIgnoreCase(p.address);
	}

	@Override
	public int hashCode() {
		return this.firstName.hashCode() ^ this.lastName.hashCode() /*^ this.address.hashCode()*/;
	}
}
