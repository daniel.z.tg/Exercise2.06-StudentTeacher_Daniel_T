import java.util.GregorianCalendar;

import java.time.LocalDate;
import java.time.Month;

import me.danielzgtg.compsci11_sem2_2017.common.Validate;
import me.danielzgtg.compsci11_sem2_2017.common.ui.LatinLanguageUtils;

@SuppressWarnings("WeakerAccess")
public final class Date {
	private volatile int year;
	private volatile Month month;
	private volatile int day;
	private volatile LocalDate javaDate = null;

	public static final int JANUARY = Month.JANUARY.ordinal();
	public static final int FEBRUARY = Month.FEBRUARY.ordinal();
	public static final int MARCH = Month.MARCH.ordinal();
	public static final int APRIL = Month.APRIL.ordinal();
	public static final int MAY = Month.MAY.ordinal();
	public static final int JUNE = Month.JUNE.ordinal();
	public static final int JULY = Month.JULY.ordinal();
	public static final int AUGUST = Month.AUGUST.ordinal();
	public static final int SEPTEMBER = Month.SEPTEMBER.ordinal();
	public static final int OCTOBER = Month.OCTOBER.ordinal();
	public static final int NOVEMBER = Month.NOVEMBER.ordinal();
	public static final int DECEMBER = Month.DECEMBER.ordinal();

	private final static GregorianCalendar CALENDAR_HELPER = new GregorianCalendar();

	public static final Date UNIX_EPOCH = new Date(1970, 1, 1);


	public Date(final int year, final int month, final int day) {
		this.year = year;
		this.month = Month.of(month);
		this.day = day;
		validateDate(year, this.month, day);
	}

	public final int getYear() {
		return this.year;
	}

	public final int getMonth() {
		return this.month.ordinal() + 1;
	}

	public final int getDay() {
		return this.day;
	}

	public synchronized final void setYear(final int year) {
		validateDate(year, this.month, this.day);
		this.year = year;
		this.javaDate = null;
	}

	public synchronized final void setMonth(final int month) {
		final Month newMonth = Month.of(month);

		validateDate(this.year, newMonth, this.day);

		this.month = Month.of(month);
		this.javaDate = null;
	}

	public synchronized final void setDay(final int day) {
		validateDate(this.year, this.month, day);
		this.day = day;
		this.javaDate = null;
	}

	private static final void validateDate(final int year, final Month month, final int day) {
		Validate.require(day > 0 && day <= month.length(CALENDAR_HELPER.isLeapYear(year)));
	}

	public final String toString() {
		return "Date: "
				+ this.day  + " of "
				+ LatinLanguageUtils.asCapitalizedWord(this.month.toString())
				+ ", " + this.year;
	}

	public final boolean equals(final Object o) {
		return o instanceof Date && this.equals((Date) o);
	}

	public final boolean equals(final Date d) {
		return d != null
				&& this.year == d.year
				//&& this.month.equals(d.month)
				&& this.month == d.month
				&& this.day == d.day;
	}

	private final LocalDate updateJavaDateCache() {
		return this.javaDate = LocalDate.of(this.year, this.month, this.day);
	}

	public final LocalDate asJavaDate() {
		return this.javaDate != null ? this.javaDate : updateJavaDateCache();
	}
}
