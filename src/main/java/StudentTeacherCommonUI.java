import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import me.danielzgtg.compsci11_sem2_2017.common.datastructure.Pair;
import me.danielzgtg.compsci11_sem2_2017.common.reflect.Attribute;
import me.danielzgtg.compsci11_sem2_2017.common.reflect.EncapsulatedAttribute;
import me.danielzgtg.compsci11_sem2_2017.common.ui.HumanDataManipulator;

/*packaged*/ final class StudentTeacherCommonUI {

	/*packaged*/ static final List<Pair<Attribute<?, ?>, HumanDataManipulator<?>>> ATTRIBUTES;

	static {
		final List<Pair<Attribute<?, ?>, HumanDataManipulator<?>>> attributes = new LinkedList<>();

		attributes.add(new Pair<>(
				new EncapsulatedAttribute<>(Person.class, "firstName", String.class),
				new HumanDataManipulator<>(
						String::valueOf,
						String::valueOf)
		));

		attributes.add(new Pair<>(
				new EncapsulatedAttribute<>(Person.class, "lastName", String.class),
				new HumanDataManipulator<>(
						String::valueOf,
						String::valueOf)
		));

		attributes.add(new Pair<>(
				new EncapsulatedAttribute<>(Person.class, "address", String.class),
				new HumanDataManipulator<>(
						String::valueOf,
						String::valueOf)
		));

		attributes.add(new Pair<>(
				new EncapsulatedAttribute<>(Person.class, "birthday", Date.class),
				new HumanDataManipulator<>(x -> {
					final String[] parts = x.split("-");
					return new Date(Integer.valueOf(parts[0]), Integer.valueOf(parts[1]), Integer.valueOf(parts[2]));
				}, x -> x.getYear() + "-" + x.getMonth() + "-" + x.getDay())
		));

		attributes.add(new Pair<>(
				new EncapsulatedAttribute<>(Student.class, "grade", int.class),
				new HumanDataManipulator<>(
						Integer::parseInt,
						String::valueOf)
		));

		attributes.add(new Pair<>(
				new EncapsulatedAttribute<>(Student.class, "studentId", String.class),
				new HumanDataManipulator<>(
						String::valueOf,
						String::valueOf)
		));

		//noinspection unchecked
		attributes.add(new Pair<>(new Attribute(Student.class, int.class, "numLates") {
			@Override
			public void set(final Object target, final Object data) throws ReflectiveOperationException {
				if (!(target instanceof Student)) {
					throw new NoSuchMethodException();
				}

				final Student student = (Student) target;
				final int numLates = (Integer) data;

				int toSet = student.getNumLates() - numLates;

				if (toSet < 0) {
					throw new ReflectiveOperationException();
				}

				for (; toSet >= 0; toSet--) {
					student.addLate();
				}
			}

			@Override
			public boolean isMutable() {
				return true;
			}

			@Override
			public Object get(final Object target) throws ReflectiveOperationException {
				if (!(target instanceof Student)) {
					throw new NoSuchMethodException();
				}

				return ((Student) target).getNumLates();
			}

			@Override
			public boolean isVisible() {
				return true;
			}
		}, new HumanDataManipulator<Integer>(Integer::parseInt, String::valueOf)
		));

		attributes.add(new Pair<>(
				new EncapsulatedAttribute<>(Teacher.class, "teachables", String.class),
				new HumanDataManipulator<>(
						String::valueOf,
						String::valueOf)
		));

		attributes.add(new Pair<>(
				new EncapsulatedAttribute<>(Teacher.class, "employeeId", String.class),
				new HumanDataManipulator<>(
						String::valueOf,
						String::valueOf)
		));

		ATTRIBUTES = Collections.unmodifiableList(attributes);
	}

	private StudentTeacherCommonUI() { throw new UnsupportedOperationException(); }
}
