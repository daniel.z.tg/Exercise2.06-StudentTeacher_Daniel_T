import me.danielzgtg.compsci11_sem2_2017.common.Validate;

/**
 * Describes one Student
 */
@SuppressWarnings({"WeakerAccess", "unused", "SameParameterValue"})
public class Student extends Person {

	/**
	 * The grade of the {@link Student}.
	 */
	private int grade;

	/**
	 * The student id of the {@link Student}.
	 */
	private String studentId;

	/**
	 * The number of times the {@link Student} has been late.
	 */
	private int numLates;

	/**
	 * Instantiates an anonymous {@link Student}.
	 */
	public Student() {
		super();

		this.setGrade(0);
		this.setStudentId("");
	}

	public Student(final String firstName, final String lastName) {
		super(firstName, lastName);

		this.setGrade(0);
		this.setStudentId("");
	}

	public Student(final String firstName, final String lastName,
			final int grade, final String studentId) {
		super(firstName, lastName);

		this.setGrade(grade);
		this.setStudentId(studentId);
	}

	public Student(final String firstName, final String lastName,
			final String address, final int grade, final String studentId) {
		super(firstName, lastName, address);

		this.setGrade(grade);
		this.setStudentId(studentId);
	}

	public Student(final String firstName, final String lastName,
			final String address, final Date birthday,
			final int grade, final String studentId) {
		super(firstName, lastName, address, birthday);

		this.setGrade(grade);
		this.setStudentId(studentId);
	}

	public int getGrade() {
		return this.grade;
	}

	public String getStudentId() {
		return this.studentId;
	}

	public int getNumLates() {
		return this.numLates;
	}

	public synchronized void setGrade(final int grade) {
		this.grade = grade;
	}

	public synchronized void setStudentId(final String studentId) {
		this.studentId = isValidId(studentId) ? studentId : Person.INVALID_ID_PLACEHOLDER;
	}

	public synchronized void addLate() {
		this.numLates++;
	}

	private boolean isValidId(final String id) {
		Validate.notNull(id);

		return id.length() == (1 + 9) && id.startsWith("S");
	}

	@Override
	public boolean equals(final Object o) {
		return o instanceof Student && this.equals((Student) o);
	}

	@Override
	public boolean equals(final Person p) {
		return p instanceof Student && this.equals((Student) p);
	}

	public boolean equals(final Student s) {
		return s != null && super.equals(s) &&
				this.grade == s.grade &&
				this.studentId.equals(s.studentId) &&
				this.numLates == s.numLates;
	}

	public String toString() {
		return "Student: firstName = " + this.getFirstName()
				+ ", lastName = " + this.getLastName()
				+ ", address = " + this.getAddress()
				+ ", birthday = " + this.getBirthday()
				+ ", age = " + this.getAge()
				+ ", grade = " + this.grade
				+ ", studentId = " + this.studentId
				+ ", numLates = " + this.numLates;
	}

	@Override
	public int hashCode() {
		return super.hashCode() ^ /*this.grade ^*/
				this.studentId.hashCode() /*^ this.numLates*/;
	}
}
