import java.time.LocalDate;
import java.time.Period;

public final class LocalUtil {

	public static final int getAge(final LocalDate birthday, final LocalDate now) {
		return Period.between(birthday, now).getYears();
	}

	private LocalUtil() { throw new UnsupportedOperationException(); }
}
